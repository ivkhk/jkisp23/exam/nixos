{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.11";
  };

  outputs = inputs@{ self, nixpkgs, ... }:

    let
      system = "x86_64-linux";
      lib = nixpkgs.lib;
      pkgs = import nixpkgs {
        inherit system;
      };
    in {
      nixosConfigurations = {

        master = nixpkgs.lib.nixosSystem {
          inherit system;
          modules = [
             ./hosts/master
          ];
        };

        worker = nixpkgs.lib.nixosSystem {
          inherit system;
          modules = [
             ./hosts/worker
          ];
        };

      };
    };
}
