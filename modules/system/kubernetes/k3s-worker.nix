{ config, pkgs, ... }:
{
  services.k3s = {
    enable = true;
    role = "agent";
    token = "8c91762d04544434500210cfbfd6cd55";
    serverAddr = "https://<MASTER_IP>:6443";
    extraFlags = "--node-name=worker --node-ip=<WORKER_IP> --node-external-ip=<WORKER_IP> --flannel-iface=ens18";
  };
}
