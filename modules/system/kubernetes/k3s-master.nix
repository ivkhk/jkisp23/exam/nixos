{ config, pkgs, ... }:
{
  services.k3s = {
    enable = true;
    role = "server";
    token = "8c91762d04544434500210cfbfd6cd55";
    clusterInit = true;
    extraFlags = "--node-name=master --bind-address=<MASTER_IP> --disable servicelb --disable traefik --tls-san <MASTER_IP> --node-ip=<MASTER_IP> --node-external-ip=<MASTER_IP> --flannel-iface=ens18";
  };
}
